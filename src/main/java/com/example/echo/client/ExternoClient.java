package com.example.echo.client;

import com.example.echo.model.Email;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Service
@FeignClient(url = "https://app-bicicletario-main-230620134153.azurewebsites.net/", name = "usuario")
public interface ExternoClient {

    @PostMapping("/enviarEmail")
     boolean enviarEmail(@RequestBody Email email);

}
